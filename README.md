# Currency Exchange

This projects uses https://currencylayer.com free api in order to calculate the following:
  - exchange rates from 1 currency to several
  - conversion from 1 currency to several
  - highest exchange rate in last 7 days

Although the requirement asked to not give the output in the console, it was the fastest way I could do it.

Before everything, it is necessary to execute `bundle install`.

The script accepts has the following help:

```
  $ruby currency_exchange.rb 
  missing --date
  Usage: currency_exchange [options]
      --token ACCESS_KEY           Access token to access the currency service
      -c, --currencies CURRENCIES      List of currencies separated by comma
      -d, --date DATE                  Date to fetch exchange rates, should be in the format: yyyy-mm-dd
      -a, --action ACTION              Action to execute: rates, convert, highest
      -v, --value VALUE                Value to convert
      -b, --base CURRENCY              Base currency
      -t, --target CURRENCY            Target currency
      -h, --help                       Help

```

The followin are examples in how to run it:

```
  $ ruby currency_exchange.rb --token TOKEN --date 2017-06-25 --base EUR --currencies BRL --action convert --value 1
    {"BRL"=>3.7416152879020785}
  
  $ ruby currency_exchange.rb --token TOKEN --date 2017-06-25 --base EUR --action highest --target BRL
    {:date=>"2017-06-24", :highest_rate=>3.741632370656647}
  
  $ ruby currency_exchange.rb --token TOKEN --date 2017-06-25 --base EUR --currencies BRL --action rates
    {"EURBRL"=>3.7415606806986115}
  
  $ ruby currency_exchange.rb --token TOKEN --date 2017-06-25 --base EUR --currencies BRL,ARS --action rates
    {"EURBRL"=>3.7415606806986115, "EURARS"=>18.095612404836544}
```

To run specs execute:
```
  $ bundle exec rspec spec/currency-exchange/
```
