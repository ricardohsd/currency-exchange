module CurrencyExchange
  class RateCalculator
    DEFAULT_CURRENCY = 'USD'

    def self.rates_for(currency:, rates:)
      new(rates: rates).rates_for(currency)
    end

    def initialize(rates:)
      @rates = rates
    end

    # Calculates and returns the rate for a currency.
    # If the given currency is USD it will return the `@rates` value.
    #
    # In order to calculate the rates based in the given currency it is
    # necessary to normalize everything based on USD rates.
    #
    # @param target_currency [String]
    # @return [Hash<String, Float>]
    def rates_for(target_currency)
      return @rates if target_currency == DEFAULT_CURRENCY

      currencies = parse_currencies(@rates.dup)
      currencies.delete(target_currency)

      calculate_rates(target_currency, currencies)
    end

    private

    def parse_currencies(rates)
      rates.keys.map do |rate|
        rate.gsub(/USD/, '')
      end.reject { |c| c.empty? }
    end

    def calculate_rates(target_currency, currencies)
      currencies.each_with_object({}) do |currency, new_rates|
        new_rates["#{target_currency}#{currency}"] = (1.0 / @rates["USD#{target_currency}"]) / (1.0 / @rates["USD#{currency}"])
      end
    end
  end
end
