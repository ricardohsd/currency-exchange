require 'json'
require 'net/http'
require 'uri'
require 'date'

module CurrencyExchange
  class Api
    class InvalidDate < StandardError; end

    URL = 'http://apilayer.net/api/historical'

    def self.fetch(access_key:, date:, currencies:)
      new(access_key: access_key, date: date, currencies: currencies).fetch
    end

    def initialize(access_key:, date:, currencies:)
      @access_key = access_key
      @date = date
      @currencies = currencies

      valid_date?
    end

    def fetch
      url = build_url
      response = Net::HTTP.get(url)

      JSON(response)
    end

    private

    def valid_date?
      Date.strptime(@date, '%Y-%m-%d')
    rescue
      raise InvalidDate.new("The following value '#{@date}' isn't a valid date value")
    end

    def build_url
      params = {
        access_key: @access_key,
        date: @date,
        currencies: @currencies.join(',')
      }.map do |key, value|
        "#{key}=#{value}"
      end.join('&')

      URI("#{URL}?#{params}")
    end
  end
end
