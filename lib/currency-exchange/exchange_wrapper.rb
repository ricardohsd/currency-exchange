require 'date'
require './lib/currency-exchange/rate_calculator'
require './lib/currency-exchange/api'

module CurrencyExchange
  class ExchangeWrapper
    def initialize(access_key:, date:)
      @access_key = access_key
      @date = date
    end

    # Returns the exchange rate of a given currency into one or more target currencies.
    #
    # It uses RateCalculator to calculate the proper exchange rate and filters the results.
    #
    # @param currency [String]
    # @param target_currencies [Array<String>]
    # @return [Hash<String, Float>]
    def exchange_rates(currency:, target_currencies:, overwrite_date: nil)
      response = api_response(target_currencies.dup << currency, overwrite_date || @date)

      rates = RateCalculator.rates_for(currency: currency, rates: response['quotes'])

      target_currencies.each_with_object({}) do |key, hs|
        currency_exchange = "#{currency}#{key}"

        hs[currency_exchange] = rates[currency_exchange]
      end
    end

    # Converts a value of given currency into one or multiple target currencies.
    #
    # @param value [Float]
    # @param currency [String]
    # @param target_currencies [Array<String>]
    # @return [Hash<String, Float>]
    def convert(value:, currency:, target_currencies:)
      rates = exchange_rates(currency: currency, target_currencies: target_currencies)

      target_currencies.each_with_object({}) do |key, hs|
        hs[key] = rates["#{currency}#{key}"] * value
      end
    end

    # Return the highest exchange rate and corresponding date of the last seven days
    # for a given currency base and target currency.
    #
    # @param base [String]
    # @param target [String]
    # @return [Hash<String, Float|Date>]
    def highest_rate(base:, target:)
      today = Date.today
      highest_rate = 0
      highest_date = nil

      (0..7).each do |offset|
        date = today - offset
        rates = exchange_rates(currency: base, target_currencies: [target], overwrite_date: date.to_s)
        exchange_rate = rates["#{base}#{target}"]

        if exchange_rate > highest_rate
          highest_rate = exchange_rate
          highest_date = date.to_s
        end
      end

      {
        date: highest_date,
        highest_rate: highest_rate
      }
    end

    private

    def api_response(currencies, date)
      Api.fetch(access_key: @access_key, date: date, currencies: currencies)
    end
  end
end
