require 'bundler'
require 'date'

Bundler.require(:default, :test)

RSpec.configure do |config|
  config.before(:each) do
    Timecop.freeze(DateTime.new(2017, 6, 25, 12, 0, 0))
  end

  config.after(:each) do
    Timecop.return
  end
end
