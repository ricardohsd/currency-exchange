require 'spec_helper'
require './lib/currency-exchange/rate_calculator'

RSpec.describe CurrencyExchange::RateCalculator do
  describe '#calculate' do
    it 'uses USD rates to calculate the rate exchange for EUR to BRL' do
      rates = { 'USDEUR' => 0.893104, 'USDBRL' => 3.295904, 'USDARS' => 15.910402 }

      calculator = CurrencyExchange::RateCalculator.new(rates: rates)

      expect(calculator.rates_for('EUR')).to eq({ 'EURBRL' => 3.6903921603754997, 'EURARS' => 17.814724824880415})
      expect(calculator.rates_for('BRL')).to eq({ 'BRLEUR' => 0.2709739118615105, 'BRLARS' => 4.827325674534209})
    end

    it 'should calculate properly USD exchange rates' do
      rates = { 'USDEUR' => 0.893104, 'USDBRL' => 3.295904, 'USDARS' => 15.910402 }

      calculator = CurrencyExchange::RateCalculator.new(rates: rates)

      expect(calculator.rates_for('USD')).to eq({ 'USDEUR' => 0.893104, 'USDBRL' => 3.295904, 'USDARS' => 15.910402 })
    end
  end
end
