require 'spec_helper'
require './lib/currency-exchange/api'

RSpec.describe CurrencyExchange::Api do
  let(:access_key) { 'aaccesskey' }

  describe '#fetch' do
    it 'raises an exception when date is in the wrong format' do
      expect do
        CurrencyExchange::Api.new(
          access_key: access_key,
          date: 'invalid-date',
          currencies: ['EUR']
        )
      end.to raise_error(CurrencyExchange::Api::InvalidDate, "The following value 'invalid-date' isn't a valid date value")
    end

    it 'fetches currency exchange rates from remote API' do
      api = CurrencyExchange::Api.new(
        access_key: access_key,
        date: '2017-06-23',
        currencies: ['EUR']
      )

      allow(Net::HTTP).to receive(:get).
        with(URI("http://apilayer.net/api/historical?access_key=aaccesskey&date=2017-06-23&currencies=EUR")).
        and_return(File.read('./spec/fixtures/request.json'))

      expect(api.fetch).to eq({
        "success" => true,
        "terms" => "https://currencylayer.com/terms",
        "privacy" => "https://currencylayer.com/privacy",
        "historical" => true,
        "date" => "2017-06-10",
        "timestamp" => 1497139199,
        "source" => "USD",
        "quotes" => {
          "USDUSD" => 1,
          "USDEUR" => 0.893104,
          "USDARS" => 15.910402,
          "USDBRL" => 3.295904
        }
      })
    end
  end
end
