require 'spec_helper'
require './lib/currency-exchange/exchange_wrapper'

RSpec.describe CurrencyExchange::ExchangeWrapper do
  let(:access_key) { 'akey' }

  describe '#exchange_rates' do
    it 'returns 1 exchange rate of a given currency' do
      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-23', currencies: ['EUR', 'BRL']).
        and_return(JSON(File.read('./spec/fixtures/request.json')))

      wrapper = CurrencyExchange::ExchangeWrapper.new(access_key: access_key, date: '2017-06-23')

      expect(wrapper.exchange_rates(currency: 'BRL', target_currencies: ['EUR'])).to eq(
        'BRLEUR' => 0.2709739118615105
      )
    end

    it 'returns multiple exchange rates of a given currency' do
      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-23', currencies: ['EUR', 'ARS', 'BRL']).
        and_return(JSON(File.read('./spec/fixtures/request.json')))

      wrapper = CurrencyExchange::ExchangeWrapper.new(access_key: access_key, date: '2017-06-23')

      expect(wrapper.exchange_rates(currency: 'BRL', target_currencies: ['EUR', 'ARS'])).to eq(
        'BRLARS' => 4.827325674534209,
        'BRLEUR' => 0.2709739118615105
      )
    end
  end

  describe '#convert' do
    it 'converts a value based on multiple exchange rates' do
      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-23', currencies: ['EUR', 'ARS', 'BRL']).
        and_return(JSON(File.read('./spec/fixtures/request.json')))

      wrapper = CurrencyExchange::ExchangeWrapper.new(access_key: access_key, date: '2017-06-23')

      expect(wrapper.convert(value: 100, currency: 'BRL', target_currencies: ['EUR', 'ARS'])).to eq(
        'ARS' => 482.7325674534209,
        'EUR' => 27.09739118615105
      )

      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-23', currencies: ['ARS', 'BRL', 'EUR']).
        and_return(JSON(File.read('./spec/fixtures/request.json')))

      expect(wrapper.convert(value: 100, currency: 'EUR', target_currencies: ['ARS', 'BRL'])).to eq(
        'ARS' => 1781.4724824880414,
        'BRL' => 369.03921603755
      )
    end
  end

  describe '#highest_rate' do
    it 'returns the best exchange rate in the last seven days for USD' do
      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: anything, currencies: ['EUR', 'USD']).
        and_return(JSON(File.read('./spec/fixtures/request.json')))

      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-25', currencies: ['EUR', 'USD']).
        and_return(JSON(File.read('./spec/fixtures/request-2016-06-25.json')))

      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-24', currencies: ['EUR', 'USD']).
        and_return(JSON(File.read('./spec/fixtures/request-2016-06-24.json')))


      wrapper = CurrencyExchange::ExchangeWrapper.new(access_key: access_key, date: '2017-06-23')

      expect(wrapper.highest_rate(base: 'USD', target: 'EUR')).to eq(
        date: '2017-06-24',
        highest_rate: 0.893304
      )
    end

    it 'returns the best exchange rate in the last seven days for BRL' do
      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: anything, currencies: ['EUR', 'BRL']).
        and_return(JSON(File.read('./spec/fixtures/request.json')))

      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-25', currencies: ['EUR', 'BRL']).
        and_return(JSON(File.read('./spec/fixtures/request-2016-06-25.json')))

      allow(CurrencyExchange::Api).to receive(:fetch).
        with(access_key: access_key, date: '2017-06-24', currencies: ['EUR', 'BRL']).
        and_return(JSON(File.read('./spec/fixtures/request-2016-06-24.json')))

      wrapper = CurrencyExchange::ExchangeWrapper.new(access_key: access_key, date: '2017-06-23')

      expect(wrapper.highest_rate(base: 'BRL', target: 'EUR')).to eq(
        date: '2017-06-23',
        highest_rate: 0.2709739118615105
      )
    end
  end
end
