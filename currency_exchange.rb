#!/usr/bin/ruby -w

require 'optparse'
require './lib/currency-exchange/exchange_wrapper'

@options = {}

@cli = OptionParser.new do |opts|
  opts.on('-t', '--token ACCESS_KEY', 'Access token to access the currency service') do |token|
    @options[:token] = token
  end

  opts.on('-c', '--currencies CURRENCIES', 'List of currencies separated by comma') do |currencies|
    @options[:currencies] = currencies.split(',').map(&:strip)
  end

  opts.on('-d', '--date DATE', 'Date to fetch exchange rates, should be in the format: yyyy-mm-dd') do |date|
    @options[:date] = date
  end

  opts.on('-a', '--action ACTION', 'Action to execute: rates, convert, highest') do |action|
    @options[:action] = action
	end

  opts.on('-v', '--value VALUE', 'Value to convert') do |value|
    @options[:value] = value.to_f
	end

  opts.on('-b', '--base CURRENCY', 'Base currency') do |base|
    @options[:base] = base
	end

  opts.on('-ta', '--target CURRENCY', 'Target currency') do |target|
    @options[:target] = target
	end

  opts.on('-h', '--help', 'Help') do
    @options[:help] = true
  end
end

@cli.parse!

def field_required!(field)
  unless @options[field]
    puts "missing --#{field}"
    puts @cli

    exit 1
  end
end

if @options[:help]
  puts @cli

  exit 0
end

field_required!(:date)
field_required!(:token)
field_required!(:base)

wrapper = CurrencyExchange::ExchangeWrapper.new(access_key: @options[:token], date: @options[:date])

case @options[:action]
when 'rates'
  if @options.fetch(:currencies, []).empty?
    puts 'missing --currencies'
    puts @cli

    exit 1
  end

  result = wrapper.exchange_rates(currency: @options[:base], target_currencies: @options[:currencies])

  puts result.inspect
when 'convert'
  field_required!(:value)

  if @options.fetch(:currencies, []).empty?
    puts 'missing --currencies'
    puts @cli

    exit 1
  end

  result = wrapper.convert(value: @options[:value], currency: @options[:base], target_currencies: @options[:currencies])

  puts result.inspect
when 'highest'
  field_required!(:target)

  result = wrapper.highest_rate(base: @options[:base], target: @options[:target])

  puts result.inspect
else
  puts "invalid action #{@options[:action]}"
  puts @cli

  exit 1
end
